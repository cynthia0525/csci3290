# CSCI3290 	Computational Imaging and Vision (Spring 2020)
It is an emerging new field created by the convergence of computer graphics, computer vision and machine learning. Its main purpose is to overcome the limitations of the traditional camera by using computational techniques to produce a richer, more vivid, perhaps more perceptually meaningful representation of our visual world. The content of this course is to study ways in which samples from the real world (images and video) can be used to generate compelling computer imagery. We will learn how to acquire, represent, and render scenes from digitized photographs. The following topics will be covered: cameras, image formation and models; image manipulation (warping, morphing, mosaicing, matting, compositing); data-driven synthesis; visual perception; high dynamic range imaging and tone mapping; image-based lighting; non-photorealistic rendering; and other applications in machine vision.

# Grading
* 10% Assignment 1 [100/100]
* 30% Assignment 2 [100/100]
* 30% Assignment 3 [115/120]
* 30% Assignment 4 [89/100]

# Remarks
Please follow the instructions mentioned in the specification to compile and execute the programs.