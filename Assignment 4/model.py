#
# CSCI3290 Computational Imaging and Vision *
# --- Declaration --- *
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/ *
# Assignment 4
# Name : CHAN Ka Yi
# Student ID : 1155093641
# Email Addr : 1155093641@link.cuhk.edu.hk
#

import torch.nn as nn


class SRCNN(nn.Module):
    def __init__(self):
        super(SRCNN, self).__init__()
        # preprocessing
        self.upscale = nn.Upsample(scale_factor=3, mode='bicubic')
        # patch extraction
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=9, padding=4, stride=1)
        self.acti1 = nn.ReLU()
        # non-linear mapping
        self.conv2 = nn.Conv2d(in_channels=64, out_channels=32, kernel_size=1, padding=0, stride=1)
        self.acti2 = nn.ReLU()
        # reconstruction
        self.conv3 = nn.Conv2d(in_channels=32, out_channels=3, kernel_size=5, padding=2, stride=1)

    def forward(self, x):
        # preprocessing
        scaled = self.upscale(x)
        # patch extraction
        layer1 = self.conv1(scaled)
        layer1 = self.acti1(layer1)
        # non-linear mapping
        layer2 = self.conv2(layer1)
        layer2 = self.acti2(layer2)
        # reconstruction
        layer3 = self.conv3(layer2)
        return layer3
